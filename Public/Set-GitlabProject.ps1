﻿function Set-GitlabProject
{
    <#
      .SYNOPSIS
      Sets the properties you specify on a project.
      .DESCRIPTION
      The Set-GitLabProject function sets the properties of the project you specify.
      returns modified project when -PassThru is specified
      .EXAMPLE
      Set-GitLabProject -ProjectID 20 -Name 'New Project Name'
      ---------------------------------------------------------------
      Sets the name for project 20 to 'New Project Name'
      .EXAMPLE
      Set-GitLabProject -ProjectID 20 -visibility_level 10
      ---------------------------------------------------------------
      Sets the visibility level of project 20 to 10
    #> 
    [CmdletBinding(DefaultParameterSetName='VisibilityCustom')]
    [Alias()]
    [OutputType()]
    Param
    (
        # The Project ID
        [Parameter(HelpMessage='The Project ID',
                   Mandatory=$true)]
    [Alias('ID')]
    [int]$ProjectID,

        # The Name of the project.
        [Parameter(HelpMessage='new project name',
                   Mandatory=$false)]
        [string]$Name,

        # Custom repository name for the project.
        [Parameter(HelpMessage='Custom repository name for the project.',
                   Mandatory=$false)]
        [string]$Path,

        # Change default branch to specified branch
        [Parameter(HelpMessage='default branch for the project',
                   Mandatory=$false)]
        [string]$DefaultBranch,

        # Short project description
        [Parameter(HelpMessage='short project description',
                   Mandatory=$false)]
        [string]$Description,

        # Specify if issues are enabled for this project
        [Parameter(HelpMessage='Are issues enabled for this project',
                   Mandatory=$false)]
        [boolean]$IssuesEnabled,

        # Specify if Merge Requests are enabled for this project
        [Parameter(HelpMessage='Are Merge Requests enabled for this project',
                   Mandatory=$false)]
        [boolean]$MergeRequestsEnabled,

        # Specify if builds are enabled for this project
        [Parameter(HelpMessage='Are Builds enabled for this project',
                   Mandatory=$false)]
        [boolean]$BuildsEnabled,

        # Specify if a wiki is enabled for this project
        [Parameter(HelpMessage='is the wiki enabled for this project',
                   Mandatory=$false)]
        [boolean]$WikiEnabled,

        # Specify if snippets are enabled for this project
        [Parameter(HelpMessage='are snippets enabled for this project',
                   Mandatory=$false)]
        [boolean]$SnippetsEnabled,

        # Specify if Issues are enabled for this project
        [Parameter(HelpMessage='are issues enabled for this project',
                   Mandatory=$false)]
        [boolean]$ContainerRegistryEnabled,

        # Specify if Shared runners are enabled for this project
        [Parameter(HelpMessage='are shared runners enabled for this project',
                   Mandatory=$false)]
        [boolean]$SharedRunnersEnabled,

        # Specify Project Visibility
        # Private. visibility_level is 0. Project access must be granted explicitly for each user. 
        # Internal. visibility_level is 10. The project can be cloned by any logged in user. 
        # Public. visibility_level is 20. The project can be cloned without any authentication.
        [Parameter(ParameterSetName = 'VisibilityCustom',
                    HelpMessage = "Private - Project access must be granted explicitly for each user. `r`n Internal - The project can be cloned by any logged in user. `r`n Public - The project can be cloned without any authentication.",
                    Mandatory = $false)]
        [validateset('Private','Internal','Public')]
        [string]$VisibilityLevel,

        # Is Visibility Public, if true same as setting visibility_level = 20
        [Parameter(ParameterSetName = 'VisibilityPublic',
                    HelpMessage='if true same as setting visibility_level = 20',
                   Mandatory=$false)]
        [switch]$Public,

        # Specify if builds are publicly accessible
        [Parameter(HelpMessage='are build public',
                   Mandatory=$false)]
        [boolean]$PublicBuilds,

        # Existing GitlabConnector Object, can be retrieved with Get-GitlabConnect
        [Parameter(HelpMessage = 'Specify Existing GitlabConnector',
                Mandatory = $false,
        DontShow = $true)]
        [psobject]$GitlabConnect = (Get-GitlabConnect),

        # Passthru the modified project
        [Parameter(HelpMessage='Passthru the modified project',
                   Mandatory=$false)]
        [switch]$PassThru
    )
    $httpmethod = 'put'
    $apiurl = "projects/$ProjectID"
    $parameters =@{}
    #name
    if($Name){
      $parameters.'name' = $Name
    }
    #path
    if($Path){
      $parameters.path = $Path
    }
    #default_branch
    if($DefaultBranch){
      $parameters.'default_branch' = $DefaultBranch
    }
    #description
    if($PSCmdlet.MyInvocation.BoundParameters.keys -contains 'Description'){
      if($Description){
        $parameters.description = 'true'
      }
      else{
        $parameters.description = 'false'
      }
    }
    #issues_enabled
    if($PSCmdlet.MyInvocation.BoundParameters.keys -contains 'IssuesEnabled'){
      if($IssuesEnabled){
        $parameters.issues_enabled = 'true'
      }
      else{
        $parameters.issues_enabled = 'false'
      }
    }
    #merge_requests_enabled
    if($PSCmdlet.MyInvocation.BoundParameters.keys -contains 'MergeRequestsEnabled'){
      if($MergeRequestsEnabled){
        $parameters.merge_requests_enabled = 'true'
      }
      else{
        $parameters.merge_requests_enabled = 'false'
      }
    }
    #builds_enabled
    if($PSCmdlet.MyInvocation.BoundParameters.keys -contains 'BuildsEnabled'){
      if($BuildsEnabled){
        $parameters.builds_enabled = 'true'
      }
      else{
        $parameters.builds_enabled = 'false'
      }
    }
    #wiki_enabled
    if($PSCmdlet.MyInvocation.BoundParameters.keys -contains 'WikiEnabled'){
      if($WikiEnabled){
        $parameters.wiki_enabled = 'true'
      }
      else{
        $parameters.wiki_enabled = 'false'
      }
    }
    #snippets_enabled
    if($PSCmdlet.MyInvocation.BoundParameters.keys -contains 'SnippetsEnabled'){
      if($SnippetsEnabled){
        $parameters.snippets_enabled = 'true'
      }
      else{
        $parameters.snippets_enabled = 'false'
      }
    }
    #container_registry_enabled
    if($PSCmdlet.MyInvocation.BoundParameters.keys -contains 'ContainerRegistryEnabled'){
      if($ContainerRegistryEnabled){
        $parameters.container_registry_enabled = 'true'
      }
      else{
        $parameters.container_registry_enabled = 'false'
      }
    }
    #shared_runners_enabled
    if($PSCmdlet.MyInvocation.BoundParameters.keys -contains 'SharedRunnersEnabled'){
      if($SharedRunnersEnabled){
        $parameters.shared_runners_enabled = 'true'
      }
      else{
        $parameters.shared_runners_enabled = 'false'
      }
    }
    #visibility_level
    if($VisibilityLevel){
		if($VisibilityLevel -eq 'Public'){$parameters.'visibility_level' = 20}
		Elseif($VisibilityLevel -eq 'Internal'){$parameters.'visibility_level' = 10}
		Else{$parameters.'visibility_level' = 0}
    }
    #public
    if($PSCmdlet.MyInvocation.BoundParameters.keys -contains 'Public'){
      if($Public){
        $parameters.public = 'true'
      }
      else{
        $parameters.public = 'false'
      }
    }
    #public_builds
    if($PSCmdlet.MyInvocation.BoundParameters.keys -contains 'PublicBuilds'){
      if($PublicBuilds){
        $parameters.public_builds = 'true'
      }
      else{
        $parameters.public_builds = 'false'
      }
    }




    $newproj = $GitlabConnect.callapi($apiurl,$httpmethod,$parameters)

    if($PassThru){
        return $newproj
    }
    
}